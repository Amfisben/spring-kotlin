package devmark.springkotlin.service

import devmark.springkotlin.dto.CountryDto

interface CountryService {
    fun getAll () : List<CountryDto>
}