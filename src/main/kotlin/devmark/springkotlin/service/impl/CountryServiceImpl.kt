package devmark.springkotlin.service.impl

import org.springframework.stereotype.Service
import devmark.springkotlin.dto.CountryDto
import devmark.springkotlin.service.CountryService

@Service
class CountryServiceImpl : CountryService
{
    override fun getAll(): List<CountryDto> {
        return listOf(
            CountryDto(1, "Германия", 10_000_000),
            CountryDto(2, "Франиця", 15_000_000),
            CountryDto(3, "Англия", 12_000_000),
        )
    }
}