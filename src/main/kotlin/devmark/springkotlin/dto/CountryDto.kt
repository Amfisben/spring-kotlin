package devmark.springkotlin.dto

data class CountryDto(
    val id: Int,
    val name: String,
    val population: Int,
)
