package devmark.springkotlin.controller

import devmark.springkotlin.dto.CountryDto
import devmark.springkotlin.service.CountryService
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController



@RestController
@RequestMapping("/countries")
class CountryController (
    private val countryService: CountryService,
){
    @GetMapping
    fun getAll() : List<CountryDto> = countryService.getAll()
}